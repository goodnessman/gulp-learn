var gulp = require('gulp'),
	uglify = require('gulp-uglify'),
	sass = require('gulp-ruby-sass'),
	autoprefixer = require('gulp-autoprefixer'),
	plumber = require('gulp-plumber');

// Script Task
// Uglifies
gulp.task('scripts', function() {
	gulp.src('app/js/*.js')
	.pipe(plumber())
	.pipe(uglify())
	.pipe(gulp.dest('app/minify/js'));
});

// Stales Task
// Uglifies
gulp.task('styles', function() {
	return sass('app/sass/*.scss', {
		style: 'compressed'
	})
	.on('error', sass.logError)
	.pipe(gulp.dest('app/minify/css/'));
});



// Watch Task
// Watch JS
gulp.task('watch', function() {
	gulp.task('watch', function() {
		gulp.watch('app/js/*.js', ['scripts']);
		gulp.watch('app/sass/*.scss', ['styles']);
	});
});

gulp.task('default', ['scripts', 'styles', 'watch']);